package com.example.android.bustracker01_beta;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ListPopupWindow;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import com.example.android.bustracker01_beta.data.BusContract;
import com.example.android.bustracker01_beta.data.BusdbHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;

public class BusActivity extends AppCompatActivity {
    public BusdbHelper mBusdbHelper = new BusdbHelper(this);
    // The to the server
    private static final String OURSERVER = "http://bus.toile-libre.org/dev/sys";
    private String HashCode = "";
    private ArrayList<String> stationName = new ArrayList<>();
    private ArrayList<String> Bus = new ArrayList<String>();
    // DATA that we will send to the result activity
    private String stationItem ;
    private String busItem ;
    // Definig Views
    private Button submitButton ;
    private   Spinner mSpinner1,mSpinner ;
    private Switch mToggleButtonDirection ;



    private void GetStationName(){
       SQLiteDatabase db =  mBusdbHelper.getReadableDatabase();
       String[] projection = {BusContract.BusEntry.COLUMN_STATION_NAME};
        Cursor cursor = db.query(BusContract.BusEntry.TABLE_NAME,projection,null,null,null,null,null,null);
        for (int i = 0 ; i< cursor.getCount();i++)
        { cursor.moveToFirst();
                do{
                    int nomIndex = cursor.getColumnIndex(BusContract.BusEntry.COLUMN_STATION_NAME);
                    String data = cursor.getString(nomIndex);
                    try {
                       stationName.add(data);
                   }catch (NullPointerException e)
                   {
                       Toast.makeText(this,"we canonot",Toast.LENGTH_SHORT);
                   }
                }while(cursor.moveToNext());
            }
        cursor.close();

    }

   private void UpdateUI(){
       // Spinner Code
       // We define the spinner
       mSpinner = (Spinner) findViewById(R.id.sort_by_spinner);
       ArrayAdapter<String> spin_adapter = new ArrayAdapter<String>(BusActivity.this, android.R.layout.simple_spinner_item, Bus);
       // setting adapteers to spinners
       // Drop down layout style - list view with radio button
       spin_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
       mSpinner.setAdapter(spin_adapter);

       mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
           @Override
           public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                busItem = adapterView.getSelectedItem().toString();
           }
           @Override
           public void onNothingSelected(AdapterView<?> adapterView) {
           //TODO : if nothing is selected do nothing  :) hhhh
           }
       } );


       mSpinner1 = (Spinner) findViewById(R.id.sort_by_spinner1_);
       mSpinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
           @Override
           public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
           stationItem = adapterView.getSelectedItem().toString();
           }
           @Override
           public void onNothingSelected(AdapterView<?> adapterView) {
           }
       } );
       ArrayAdapter<String> spin_adapter1 = new ArrayAdapter<String>(BusActivity.this, android.R.layout.simple_spinner_item, stationName);
       // setting adapteers to spinners
       spin_adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        try {
            Field popup = Spinner.class.getDeclaredField("mpoppup");
            popup.setAccessible(true);
            ListPopupWindow popupWindow = (ListPopupWindow) popup.get(mSpinner);
            popupWindow.setHeight(500);
        }catch (NoClassDefFoundError | ClassCastException | NoSuchFieldException |  IllegalAccessException e  ){}
       mSpinner1.setAdapter(spin_adapter1);
       // Button Submit
       submitButton = (Button) findViewById(R.id.submit_button);

       submitButton.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
            Intent i = ResultActivity.newIntent(BusActivity.this,stationItem,busItem);
             startActivity(i);
           }
       });

   }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bus);
        Bus.add("Ligne 1");
        new GetData().execute();
        GetStationName();
        UpdateUI();


    }

    private class GetData extends AsyncTask<Void, Void, Void> {
        // We Create a instace on BusdbHelper
        SQLiteDatabase db = mBusdbHelper.getWritableDatabase();
        // All we need to do Parsing JSON
        private HttpHandler sh = new HttpHandler();
        private String url = OURSERVER + "/GetData.php?ligne=TGM";
        private String HashUrl = OURSERVER + "/GetData.php?ligne=TGM&hash";
        private String TimeUrl = OURSERVER + "/GetBlocNFO.php?ligne=TGM";
        private String NewHashCode = sh.makeServiceCall(HashUrl);
        private String jsonStr;
        private String jsonStrTime;
        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Toast.makeText(BusActivity.this, "JSON data is Downding", Toast.LENGTH_SHORT).show();

            pDialog = new ProgressDialog(BusActivity.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            if (NewHashCode == HashCode) {
                jsonStr = "AlreadyLoaded";
            } else {
                HashCode = NewHashCode;
                jsonStr = sh.makeServiceCall(url);
                jsonStrTime = sh.makeServiceCall(TimeUrl);
            }

            // import stations'  infos
            if ((jsonStr != null) && (jsonStr != "AlreadyLoaded")) {
                try {
                    // All Station Information from the GetDATA JSON
                   final  JSONObject jsonObject = new JSONObject(jsonStr);
                    final JSONArray node = jsonObject.getJSONArray("Station");
                    // All station Information from the TimeData JSON
                    JSONObject jsonObject1 = new JSONObject(jsonStrTime);
                    JSONArray  Direct = jsonObject1.getJSONArray("1");
                    JSONArray Inverse = jsonObject1.getJSONArray("0");
                    for (int i = 0; i < node.length(); i++) {
                        // Getting the DATA from the JSON and put it in the DATABASE
                        ContentValues mcontent = new ContentValues();
                        JSONObject station = node.getJSONObject(i);
                        String nom_station = station.optString("Nom");
                        Double StationCoordX = station.optDouble("CoordX");
                        Double StationCoordY = station.optDouble("CoordY");
                        mcontent.put(BusContract.BusEntry.COLUMN_STATION_NAME, nom_station);
                        mcontent.put(BusContract.BusEntry.COLUMN_STAION_COORDX, StationCoordX);
                        mcontent.put(BusContract.BusEntry.COLUMN_STAION_COORDX, StationCoordY);

                        // Getting the Time info from the JSON ans pout it in the DATAASE
                        String timeDirect =  Direct.getJSONArray(i).toString();
                        String timeInverse = Inverse.getJSONArray(i).toString();
                        mcontent.put(BusContract.BusEntry.COLUMN_STATION_DEPART,timeDirect);
                        mcontent.put(BusContract.BusEntry.COLUMN_STATION_ARRIVAL,timeInverse);
                        // Put it a row
                            long currentID = db.insert(BusContract.BusEntry.TABLE_NAME, null, mcontent);
                    }
                } catch (final JSONException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), "JSON parsing error" + e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    });
                }
            } else if (jsonStr == "AlreadyLoaded") {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "File already exists ", Toast.LENGTH_LONG).show();
                    }
                });
            } else
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "We coudn't have the JSON object from the server ", Toast.LENGTH_LONG).show();
                    }
                });
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Toast.makeText(BusActivity.this, "finish...", Toast.LENGTH_SHORT);
            if (pDialog.isShowing())
                pDialog.dismiss();
        }
    }


}

