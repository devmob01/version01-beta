package com.example.android.bustracker01_beta.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Lenovo on 18/07/2017.
 */

public final class BusdbHelper extends SQLiteOpenHelper {
    private static final String SQL_CREATE_ENTRIES = "CREATE TABLE "+BusContract.BusEntry.TABLE_NAME
            +" ( "+ BusContract.BusEntry._ID +" INTEGER PRIMARY KEY, "
            +BusContract.BusEntry.COLUMN_STATION_NAME+ " TEXT, "
            + BusContract.BusEntry.COLUMN_STAION_COORDX+" REAL, "
            + BusContract.BusEntry.COLUMN_STATION_COORDY+" REAL, "
            + BusContract.BusEntry.COLUMN_STATION_ARRIVAL
            + " TEXT  ,"
            + BusContract.BusEntry.COLUMN_STATION_DEPART  + " TEXT  ); "
            ;
    private static final String SQL_DELETE_ENTRIES =  "DROP TABLE IF EXISTS " + BusContract.BusEntry.TABLE_NAME;

    public static final String DATABASE_NAME = "BusStation.db";
    private static final int SQL_VERSION = 1;

    public BusdbHelper(Context context) {
        super(context, DATABASE_NAME , null , SQL_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    { db.execSQL(SQL_CREATE_ENTRIES); }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(SQL_DELETE_ENTRIES);
        onCreate(sqLiteDatabase);
    }
}
