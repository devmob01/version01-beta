package com.example.android.bustracker01_beta;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.android.bustracker01_beta.data.BusContract;
import com.example.android.bustracker01_beta.data.BusdbHelper;

import java.util.ArrayList;

public class ResultActivity extends AppCompatActivity {
    // We define the constante String for EXTRAS
    private static final String EXTRA_DATA_Station ="com.example.android.bustracker01_beta.extra_data_station";
    private static final String EXTRA_DATA_bus = "com.example.android.bustracker01_beta.extra_data_bus";
    private BusdbHelper busdbHelper = new BusdbHelper(this);

    // ALL the Views in the result
    private TextView text1 ;
    private TextView text2 ;
    private Cursor getStationQuery(String str){
        // TODO ; Do a query to the data base and retreive the data we want
        SQLiteDatabase db = busdbHelper.getReadableDatabase();
        String selection = BusContract.BusEntry.COLUMN_STATION_NAME + " =? ";
        String[] selectionArgs = { str } ;
        Cursor cursor = db.query(BusContract.BusEntry.TABLE_NAME,null,selection,selectionArgs,null,null,null);
        return cursor ;
    }
    private void putArray (ArrayList<Integer> a ,ArrayList<Integer> b )
    {
        for ( int i = 0 ; i< a.size();i++)
            b.add(a.get(i));
    }
    public static Intent newIntent(Context PackageContext,String str , String str1 )
    {
        Intent i = new Intent(PackageContext,ResultActivity.class);
        i.putExtra(EXTRA_DATA_Station,str);
        i.putExtra(EXTRA_DATA_bus,str1);
        return i ;
    }
    private final ArrayList<String> getTimeArray (String str) {
        //TODO : this method will take a string and turn it into an array
        ArrayList<String> tab = new ArrayList<>();
        String[] aSec =str.substring(1,str.length() - 1).split(",");
        for(String ms:aSec){
            //System.out.println(ms);
            int a = Integer.parseInt(ms);

            int h = (a/3600);
            int m = (a%3600) / 60;
            int s = a%60;
            String hms = String.format("%02d",h)+":"+String.format("%02d",m)+":"+String.format("%02d",s);
            tab.add(hms);
        }
        return tab ;
    }
    private void getInfo(){
        //TODO  ; this method will extract data from the cursor
    }
    private void UpdateUI(String str , String str1){
        text1 = (TextView) findViewById(R.id.bus_name);
        text1.setText(str);
        text2 = (TextView) findViewById(R.id.station_name);
    text2.setText(str1);}
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        Intent data = getIntent();
        String str =data.getStringExtra(EXTRA_DATA_Station);
        String str1 =data.getStringExtra(EXTRA_DATA_bus);
        Cursor cursor = getStationQuery(str);
        cursor.moveToFirst();
        int index =  cursor.getColumnIndex(BusContract.BusEntry.COLUMN_STATION_ARRIVAL);
        String arrivalTime = cursor.getString(index);
        index = cursor.getColumnIndex(BusContract.BusEntry.COLUMN_STATION_DEPART);
        String departTime = cursor.getString(index);
        // List view
        ListView lv = (ListView) findViewById(R.id.listview);
        ArrayList<String> arrivalArray = getTimeArray(arrivalTime);
        ArrayAdapter marrayAdapter = new ArrayAdapter(ResultActivity.this,android.R.layout.simple_list_item_1,arrivalArray);
        lv.setAdapter(marrayAdapter);
        // list view
        ListView lv1 = (ListView) findViewById(R.id.listview1);
        ArrayList<String> departlArray = getTimeArray(departTime);
        ArrayAdapter marrayAdapter1 = new ArrayAdapter(ResultActivity.this,android.R.layout.simple_list_item_1,departlArray);
        lv1.setAdapter(marrayAdapter1);

        getInfo();
        UpdateUI(str,str1);


    }

}
