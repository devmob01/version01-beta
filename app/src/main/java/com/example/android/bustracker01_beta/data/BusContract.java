package com.example.android.bustracker01_beta.data;

import android.provider.BaseColumns;

/**
 * Created by Lenovo on 18/07/2017.
 */

public final class BusContract {
    public static final String CONTENT8_AUTHORITY ="com.example.android.bustracker01_beta";
  public  static class BusEntry implements BaseColumns {

      public static final String TABLE_NAME ="BusStation";
      public static final String _ID = BaseColumns._ID;
      public static final String   COLUMN_STATION_NAME ="name" ;
      public static final String COLUMN_STAION_COORDX = "CoordX" ;
      public static final String COLUMN_STATION_COORDY ="CoordY";
      public static final String COLUMN_STATION_ARRIVAL ="Arrival";
      public static final String COLUMN_STATION_DEPART = "Depart";
  }
}
